<p align="center">
  <h1 align="center">dash-config</h1>
  <p align="center">
    My configurations for the dash shell
  </p>
</p>

## 📰 Description

My configurations for the `dash` shell which include
POSIX interactive functions that could also be *in theory* used
by other POSIX shells like `bash`, `yash`, `mksh`, `zsh`.

*Thus, here I maintain interactive functions that don't need to be attached
to one of those specific shells. I just symlink to here.*

## 🏠 Runcom placing

Firstly, you will notice that dash doesn't normally read these files,
just `~/.profile`. What I do is start dash as a login shell (`dash -l`)
so it reads `/etc/profile`, and there set `$ENV` to `~/.config/dash/dashrc`
if is an interactive dash session, something like this:

```sh
is_interactive() { case $- in (*i*);; (*) false;; esac ;}
SHELL_CURRENT="$(readlink -f "/proc/$$/exe")"

if is_interactive && [ "$SHELL_CURRENT" = "/usr/bin/dash" ]
then
  export ENV="${HOME}/.config/dash/dashrc"
fi
```

## ⭐ Usage

Function | Description
:---:|---
`fcd` | Fuzzy `cd` to directories under current one. Needs my `ffd` script
`varcheck` | Check if variables are set, empty string or unset
`readhd` | Read the contents of a here document in `$REPLY`
`evalhd` | Eval the contents of a here document
`up` | Easily `cd` to upper directories
`cls` | `cd` + `ls`
`cll` | `cd` + `ll`
`cla` | `cd` + `la`
`assign` | Minimal safe way to "return" a value to a function caller, like bash's `declare -g`
`rest` | Minimal shell quoting algorithm, like bash's `${x@Q}` or GNU's `/bin/printf %q`

Prompt | Description
:---:|---
`prompt.sh` | A simple left, right prompt with a status bar of `PWD`. Mainly works well only in `dash`
`prompt_logo_minimal.sh` | A minimal left prompt, with OS logos

## 👀 See also

- [broot](https://dystroy.org/broot/): a more efficient fuzzy `cd` program
  - [broot-config](https://codeberg.org/tplasdio/broot-config): my `broot` config 
- [bash-config](https://codeberg.org/tplasdio/bash-config): my `bash` config 

## ⏭ Next steps

While I still use `dash`'s config as a common base for other interactive
functions of POSIX shells, I may eventually move to a more structured
solution like the [`xsh`](https://github.com/sgleizes/xsh) shell
configuration framework. Last I tried I had some issues, I just
need to read the docs, audit the code and test.
