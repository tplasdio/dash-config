# Algunos navegadores leerán estas variables (e.g. Brave) y puede que no sea lo que esperabas
proxy_ip="" \
proxy_port="" \
proxy_user="one" \
proxy_passwd="two"
if [ "$proxy_passwd" = "" ] || [ "$proxy_user" = "" ]; then
	export http_proxy="http://$proxy_ip:$proxy_port/"
else
	export http_proxy="http://$proxy_user:$proxy_passwd@$proxy_ip:$proxy_port/"
fi
export \
https_proxy="$http_proxy" \
ftp_proxy="$http_proxy" \
rsync_proxy=$http_proxy \
HTTP_PROXY="$http_proxy" \
HTTPS_PROXY="$http_proxy" \
FTP_PROXY="$http_proxy" \
RSYNC_PROXY="$http_proxy" \
NO_PROXY="localhost,127.0.0.1,localaddress,.localdomain.com" \
no_proxy="localhost,127.0.0.1,localaddress,.localdomain.com"
