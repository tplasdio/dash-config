# Aplicaciones por omisión
export \
OPENER="${OPENER:-/usr/bin/xdg-open}" \
EDITOR="${EDITOR:-/usr/bin/nvim}" \
AUDIO_PLAYER="${AUDIO_PLAYER:-"$SCRIPTS/mpvsk"}" \
VIDEO_PLAYER="${VIDEO_PLAYER:-"$SCRIPTS/mpvsk"}" \
READER="${READER:-/usr/bin/zathura}" \
TERMINAL="/usr/bin/alacritty" \
#BROWSER="/usr/bin/firefox" \
#
export \
MANPAGER="${EDITOR} +Man! -c 'nnoremap <buffer> j h'" \
#PAGER="${EDITOR} -" \
#
