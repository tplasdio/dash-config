# fzf configuration
export \
FZF_DEFAULT_COMMAND="fd --hidden --color=never" \
FZF_DEFAULT_OPTS="--cycle --info=inline --reverse \
--color 'hl:-1:underline,hl+:-1:underline' \
--bind 'alt-q:abort' \
--bind 'alt-bs:delete-char' \
--bind 'alt-p:toggle-preview' \
--bind 'alt-a:toggle-all' \
--bind 'ctrl-a:deselect-all' \
--bind 'alt-0:first' \
--bind 'alt-1:change-preview-window(left)' \
--bind 'alt-2:change-preview-window(up)' \
--bind 'alt-3:change-preview-window(down)' \
--bind 'alt-4:change-preview-window(right)' \
--bind 'alt-k:up' \
--bind 'alt-l:down' \
--bind 'alt-j:backward-char' \
--bind 'alt-ñ:forward-char' \
--bind 'alt-{:page-up' \
--bind 'alt-}:page-down' \
--bind 'alt-,:preview-up' \
--bind 'alt-.:preview-down' \
--tabstop=4 --preview-window=noborder --marker='🚩' --pointer='⮞' --prompt='⮞ ' \
--color=bg+:#343d46,gutter:-1,pointer:#ff3c3c,info:#ffbf1c,hl+:#ff5218,hl:#00FF5F,border:#3E1CFF,prompt:#6a4cff"
