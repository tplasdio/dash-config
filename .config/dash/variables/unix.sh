# Common environment variables for Unix systems and desktops (X11, Qt, Gtk, etc.)

export TMPDIR='/tmp'
#export XAUTHORITY="${HOME}/.Xauthority"                        # De ↓ causar problemas, pon este lugar que es el más común para el Xauthority
export XAUTHORITY="${XDG_RUNTIME_DIR}/xauthority"               # Debe estar en ese lugar al iniciar X, con startx/xinit, o gestor de pantalla
export XINITRC="${XDG_CONFIG_HOME}/X11/xinitrc"                 # Shell script leído por startx/xinit o gestor de pantalla
export XSERVERRC="${XDG_CONFIG_HOME}/X11/xserverrc"             # Shell script del comando para iniciar el servidor X
export XMODMAP="${XDG_CONFIG_HOME}/X11/xmodmap"
export XCOMPOSEFILE="${XDG_CONFIG_HOME}/X11/xcompose"
export XCOMPOSECACHE="${XDG_CACHE_HOME}/X11/xcomposecache/"
#export GTK_IM_MODULE=xim                                       # Para que Gtk use X input method (XIM) y lea el xcompose
export XENVIRONMENT="${XDG_CONFIG_HOME}/Xresources/Xresources"  # Fichero de recursos de aplicaciones en X11
export XDG_CURRENT_DESKTOP='KDE'
#export XCURSOR_THEME=Breeze_Snow
#export XCURSOR_SIZE=48
#export XCURSOR_PATH=${XDG_DATA_HOME}/icons
#export XCURSOR_DISCOVER=1 # Para sacar un hash del cursor y que sea descubrible en conexiones remotas
#XFILESEARCHPATH
#XUSERFILESEARCHPATH
#XAPPLRESDIR
#export DE='kde'
#export QT_SELECT='5'
export QT_QPA_PLATFORMTHEME='qt5ct'
#export QT_STYLE_OVERRIDE=''
