# Some variables for adjusting shells' runcom
# paths to an XDG location
# Maybe this should be set individually by each shell?

export \
DASHDOTDIR="${XDG_CONFIG_HOME}/dash" \
BASHDOTDIR="${XDG_CONFIG_HOME}/bash" \
ZDOTDIR="${XDG_CONFIG_HOME}/zsh" \
#
export \
DASHRC="${DASHDOTDIR}/dashrc" \
BASHRC="${BASHDOTDIR}/bashrc" \
ZSHRC="${ZDOTDIR}/.zshrc" \
BASHFUNCTIONS="${BASHDOTDIR}/functions" \
DASHFUNCTIONS="${DASHDOTDIR}/functions" \
ZSHFUNCTIONS="${ZDOTDIR}/functions" \
#
