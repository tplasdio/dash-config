# Env variables for programming languages

export REFER="${XDG_DATA_HOME}/biblio"                        #Bibliografía para groff
#export PYTHONHOME="/usr/lib/python3.9"                        #Hogar de Python: Lugar de instalación
export PYTHONPATH="${HOME}/prog/python"                       #PATH de Python: Directorios de donde se puede importar
export PYTHONSTARTUP="${XDG_CONFIG_HOME}/python/pythonrc.py"  #Runcom de Python: Script que corre al inicializar REPLs (no scripts?)
export PYTHONHISTORY="${XDG_CACHE_HOME}/python/history"       #Historial de Python: Todavía no mergeado en Python para reemplazar ~/.python_history
#export R_HOME="/usr/lib/R"                                    #Hogar de R: Lugar de instalación
export R_ENVIRON="/etc/R/Renviron.site"                       #Entorno de R: Donde se pueden establecer más variables de entorno para R
export R_PROFILE="/etc/R/Rprofile.site"                       #Runcom global de R: Script que corre al inicializar scripts y REPLs a menos que --no-site-file
export R_ENVIRON_USER="${XDG_CONFIG_HOME}/r/Renviron"         #Entorno local de R: Donde se pueden establecer más variables de entorno para R
export R_PROFILE_USER="${XDG_CONFIG_HOME}/r/Rprofile"         #Runcom local de R: Script que corre al inicializar scripts y REPLs a menos que --no-init-file
export R_HISTFILE="${XDG_CACHE_HOME}/r/Rhistory"              #Historial de R
export R_HISTSIZE=5000                                        #Máximo número de entradas de historial de R
#export LUA_INIT="@${XDG_CONFIG_HOME}/lua/luarc.lua"           #Runcom de Lua, cualquier versión. Al inicializar scripts y REPLs
## ↓ Causan problemas al iniciar awesome en sesión X anidada (Xephyr)
#export LUA_PATH="${HOME}/prog/lua"                            #PATH de Lua, cualquier versión
#export LUA_PATH_5_4=""                                        #PATH de Lua5.4
export PERL5LIB="/usr/local/share/perl5:${HOME}/prog/perl:${HOME}/perl5/lib/perl5"    #PATH extra de Perl (agregados al arreglo @INC), donde Perl y CPAN encuentran módulos
export RAKULIB="${HOME}/prog/raku"                            #PATH extra de Raku, pero podés usar 'use lib "directorio"'
export BC_ENV_ARGS="-ql $(echo "${SCRIPTSLIB}"/*.bc)"         #Argumentos para bc, librerías importadas
export BC_LINE_LENGTH=0                                       #No imprimir separando líneas con \
export AWKPATH="${HOME}/prog/awk:${SCRIPTSLIB}"               # Directorios donde se buscan ficheros con --file, ó --include <fichero>
#export AWKLIBPATH="${SCRIPTSLIB}"   # Directorio donde se buscan ficheros con --load <fichero>
export M4PATH="${XDG_DATA_HOME}/m4s:${XDG_DATA_HOME}/m4s/lib"  # Directorios donde se buscan ficheros con -I <fichero> #No funciona??

#export DB_CONFIG
export JULIA_DEPOT_PATH="${XDG_DATA_HOME}/julia:${JULIA_DEPOT_PATH}"  # PATH donde ./config/startup.jl es el runcom de Julia
export MYSQL_HISTFILE="${XDG_DATA_HOME}/mysql_history"
export MYSQL_PS1="\u>>> "
export MYCLI_HISTFILE="${XDG_DATA_HOME}/mycli-history"
export REDISCLI_HISTFILE="${XDG_DATA_HOME}/redis/rediscli_history"
export REDISCLI_RCFILE="${XDG_CONFIG_HOME}/redis/redisclirc"
export OCTAVE_HISTFILE="${XDG_CACHE_HOME}/octave-hsts"
export OCTAVE_SITE_INITFILE="${XDG_CONFIG_HOME}/octave/octaverc"
export GOPATH="${XDG_DATA_HOME}/go"
export SQLITE_HISTORY="${XDG_DATA_HOME}/sqlite_history"
export NVM_DIR="${XDG_DATA_HOME}/nvm"
#sqlite3 -init "${XDG_CONFIG_HOME}"/sqlite3/sqliterc
