export \
SYSTEMD_COLORS=true \
GROFF_NO_SGR=1 \
# Para Konsole y Gnome-terminal

export \
GCC_COLORS="\
error=01;31:\
warning=01;35:\
note=01;36:\
caret=01;32:\
locus=01:\
quote=01\
"

# Para colores ANSI de jq:
# nulo:false:true:números:strings:arreglos:objetos
export \
JQ_COLORS="\
1;30:\
4;38;5;202:\
4;38;5;202:\
1;38;5;129:\
3;1;32:\
1;38;5;221:\
1;31\
"
