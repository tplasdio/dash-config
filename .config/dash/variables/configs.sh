# Variables that are part of the environment
# configuration of a program, and not
# accounted by other files in this directory

export \
VIM='/usr/share/nvim' \
VIMRUNTIME="/usr/share/nvim/runtime" \
BYOBU_CONFIG_DIR="${XDG_CONFIG_HOME}/byobu" \
INPUTRC="${XDG_CONFIG_HOME}/readline/inputrc" \
LESSKEY="${XDG_CONFIG_HOME}/lesskey" \
FFMPEG_DATADIR="${XDG_CONFIG_HOME}/ffmpeg" \
RIPGREP_CONFIG_PATH="${XDG_CONFIG_HOME}/ripgrep/ripgreprc" \
PASSWORD_STORE_DIR="${XDG_DATA_HOME}/pass" \
TESSDATA_PREFIX='/usr/share/tessdata' \
TRASHDIR="${XDG_DATA_HOME}/Trash/files" \
SYSTEMD_EDITOR="${EDITOR}" \
#
