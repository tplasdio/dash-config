# Locale variables

export \
TEXTDOMAIN="$SHELL_CURRENT" \
TEXTDOMAINDIR='/usr/share/locale' \
TZ='UTC-0' \
HISTTIMEFORMAT="%d/%m/%y %T " \
#
# LC_MESSAGES=C \
# LANG="en_US.UTF-8" \
# LANGUAGE="en_US.UTF-8" \
# LC_NUMERIC=.
