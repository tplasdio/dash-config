# Non-POSIX but widely supported characters in name
# https://pubs.opengroup.org/onlinepubs/9699919799/basedefs/V1_chap03.html#tag_03_10
alias \
.-="cd - >/dev/null" \
..="cd .." \
pantallas-kde='kscreen-doctor -oi' \
tla-nogit="tla -I .git" \
tls-L='tls --depth' \
tla-L='tla --depth' \
tdu-L='dust --depth' \
ffprobe-json="ffprobe -v quiet -print_format json -show_format -show_streams" \
ffprobe-duration="ffprobe -select_streams v:0 -show_entries stream=duration -of default=noprint_wrappers=1:nokey=1 -hide_banner" \
#
