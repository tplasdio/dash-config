# Self-contained POSIX aliases

# Main aliases
alias \
ls='lsd' \
la='ls -Alh' \
ll='ls -lh' \
dir='ls' \
vdir='ll' \
gat='src-hilite-lesspipe.sh' \
grep='grep --color=auto' \
grep2='pcre2grep --color=auto' \
egrep='grep -E --color=auto' \
fgrep='grep -F --color=auto' \
rgrep="grep -RHIni --color=auto" \
regrep="grep -ERHIni --color=auto" \
rfgrep="grep -FRHIni --color=auto" \
fzgrep="fzf --no-sort -f" \
rg1="rg --max-depth 1" \
fd1="fd --max-depth 1" \
diff='diff --color=auto' \
ip='ip -color=auto' \
t='tree -Ca' \
lf='"${LF}"' \
mpv='"${MPV}"' \
mpvimg='"${MPV}" --keep-open=yes' \
gs='git sss' \
gb='git b' \
gf='git fzf' \
tlfd='\tree -CaifFph --dirsfirst --du' \
tfd='\tree -CaifFph --dirsfirst' \
ffzf='fd --hidden --print0|fzf --read0' \
ffzf0='fd --hidden --print0|fzf --read0 --print0' \
s='scripts' \
tls='lsd --tree -A' \
tla='tls -lh' \
tdu='dust' \
ff='find -type f -iname' \
pup='pup --color' \
cdu='cdu -i -dh' \
dw='drag wget' \
p='parallel' \
ac='alacritty-color' \
clean='exiftool -All= -overwrite_original' \
mime='file --dereference -b --mime-type' \
mount2='udisksctl mount -b ' \
umount2='udisksctl unmount -b ' \
emacs='emacs -nw' \
dd='dd status=progress' \
y='ytfzf -t -f -s' \
nb='newsboat' \
croissant='~/.luarocks/bin/croissant' \
xclips='xclip -sel clip' \
columnt="column -t -s '	'" \
bottlesf='flatpak run com.usebottles.bottles' \
nvimt="nvim -c 'terminal' -c 'set showtabline=0'" \
services='systemctl list-unit-files -at service' \
servicesrun='systemctl list-units --type=service --state=running' \
discos='ls -l /dev/disk/by-*' \
brillo='xrandr --output eDP1 --brightness ' \
machines="machinectl list-images" \
ffplay="ffplay -hide_banner -autoexit" \
mv="mv -i" \
ln="ln -i" \
mkfile="install -D /dev/null" \
rsyncv='rsync -avh --progress --stats' \
pausa="killall -s STOP" \
continua="killall -s CONT" \
pc="proxychains" \
sc="systemctl" \
systemd="/lib/systemd/systemd" \
bl="bluetoothctl; pasuspender /bin/true" \
za="zathura" \
lo="libreoffice" \
xd="xdotool" \
at="argos-translate" \
sshk='ssh-keygen -t ed25519 -C "" -a 20' \
gpgk='gpg --full-generate-key --expert --pinentry-mode loopback' \
ck='printf "\033[H\033[3J"' \
c="clear" \
e="exit" \
l="luajit -e " \
dm="datamash" \
fzf='fzf --height=$(("${LINES:-$(tput lines)}"-1))' \
m4sh="m4 sh.m4 -" \
open='xdg-open . 2> /dev/null &' \
urldecode='php -r "echo urldecode(fgets(STDIN));"' \
urlencode='php -r "echo urlencode(fgets(STDIN));"' \
grubpar="grep '^GRUB_CMDLINE' /etc/default/grub" \
cxxmatrix='cxxmatrix --preserve-background ' \
ippriv="ip -br a|awk '"'END{sub("/.*","",$3); print $3}'\' \
scrcpy='scrcpy --max-fps 50 --bit-rate 30M' \
R="R --quiet" \
ptpython='ptpython --dark-bg' \
ipython='ipython --no-banner' \
julia='julia --banner=no' \
#

# Commands that take others (possible aliases) as arguments
alias \
sudo='sudo ' \
doas='doas -- ' \
xargs='xargs ' \
xargsn="xargs -d '\n' " \
xargs0="xargs -0 " \
dem='dem ' \
#
