# Aliases that depend on variables in a runcom

alias \
bashed='"${EDITOR}" "${BASHRC}"' \
bleshed='"${EDITOR}" "${BLERC}"' \
bleshed='"${EDITOR}" "${BLERC}"' \
bashso='. "${BASHRC}"' \
dash='ENV="${DASHRC}" dash' \
dashed='"${EDITOR}" "${DASHRC}"' \
dashso='. "${DASHRC}"' \
fss='findmnt -o "$fscols"' \
pss='ps -eo $pscols;ps -o $pscols|head -1' \
datef='date "${datef}"' \
#
