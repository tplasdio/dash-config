# Aliases for fixing non-XDG paths of certain commands
# * You need to first create these directories
alias \
dhex='dhex -f "${XDG_CONFIG_HOME}/dhexrc"' \
wget='wget --hsts-file="${XDG_CACHE_HOME}/wget-hsts"'
abook='abook -C "${XDG_CONFIG_HOME}/abook/abookrc" --datafile "${XDG_DATA_HOME}/abook/addressbook"' \
mongo='mongo --norc --shell "${XDG_CONFIG_HOME}/mongo/mongorc.js"' \
xboard='xboard -settingsFile "${XDG_CONFIG_HOME}/xboardrc" -saveSettingsFile "${XDG_CONFIG_HOME}/xboardrc"' \
yarn='yarn --use-yarnrc "${XDG_CONFIG_HOME}/yarn/config"' \
#
