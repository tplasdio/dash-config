# print replacements
echo(){ printf '%s\n' "$*" ;}  # Never use echo
alias \
@print='printf "\033[1;32m>>\033[m%s\033[1;32m<<\033[m\n" "$@"' \
print='printf "\033[1;32m>>\033[m%s\033[1;32m<<\033[m\n"' \
println='printf %s\\n'
